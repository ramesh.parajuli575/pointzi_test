package com.codepath.mypizza;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class HierarchicalViewIdFinder {
    public static View debugViewIds(View view, String logtag) {
//        Log.d(logtag, "ViewGroup: " + view.getClass().getSimpleName() + ",      id: " + view.getId());
        System.out.println(logtag+": "+ "ViewGroup: "+ view.getClass().getSimpleName() + ",      id: " + view.getId());
        if (view.getParent() != null && (view.getParent() instanceof ViewGroup)) {
            return debugViewIds((View)view.getParent(), logtag);
        }
        else {
            debugChildViewIds(view, logtag, 0);
            return view;
        }
    }

    private static void debugChildViewIds(View view, String logtag, int spaces) {
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup)view;
            for (int i = 0; i < group.getChildCount(); i++) {
                View child = group.getChildAt(i);
//                Log.d(logtag, spaceforLogString("view: " + child.getClass().getSimpleName() + ",      id: "  + child.getId() , spaces));
                System.out.println(logtag+": "+ spaceforLogString("view: " + child.getClass().getSimpleName() + ",      id: "  + child.getId() , spaces));
                debugChildViewIds(child, logtag, spaces + 1);
            }
        }
    }

    private static String spaceforLogString(String str, int noOfSpaces) {
        if (noOfSpaces <= 0) {
            return str;
        }
        StringBuilder builder = new StringBuilder(str.length() + noOfSpaces);
        for (int i = 0; i < noOfSpaces; i++) {
            builder.append(' ');
        }
        return builder.append(str).toString();
    }
}
